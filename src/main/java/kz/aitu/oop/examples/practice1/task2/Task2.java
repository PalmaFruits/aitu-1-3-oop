package kz.aitu.oop.examples.practice1.task2;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class Task2 {
    public static boolean fileExist(String s) {
        try{
            FileReader fileReader=new FileReader(s);
            return true;
        } catch (FileNotFoundException e){
            return false;
        }
    }
    public static boolean isInt(String s){
        try{
            int a=Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e){
            return false;
        }
    }
    public static boolean isDouble(String s){
        try {
            double a=Double.parseDouble(s);
            return true;
        } catch (NumberFormatException e){
            return false;
        }
    }
}
