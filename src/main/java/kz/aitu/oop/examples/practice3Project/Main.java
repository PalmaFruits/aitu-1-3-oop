package kz.aitu.oop.examples.practice3Project;

import java.sql.*;

public class Main {

    public static void main(String[] args) {
        String url="jdbc:mysql://localhost:3306/pract3";
        String username="Didar";
        String password="d1freez14";
        Project project = new Project();
        try{
            Connection connection = DriverManager.getConnection(url,username,password);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select title from project where id=1");
            if(resultSet.next()){
                project = new Project(resultSet.getString("title"));
            }

            ResultSet resSet = statement.executeQuery("select name,salary,progDegree,manType from project join " +
                    "projectemp p on project.id = p.proj_id join employee e on p.emp_id = e.id where title='" +
                    project.getTitle()+"'");
            while (resSet.next()){
                if(resSet.getString("progDegree")==null){ //if progDegree is null, people is manager
                    Employee employee = new Manager(resSet.getString("name"),resSet.getInt("salary"),resSet.getString("manType"));
                    project.addEmployee(employee);
                }
                else {
                    Employee employee = new Programmer(resSet.getString("name"),resSet.getInt("salary"),resSet.getString("progDegree"));
                    project.addEmployee(employee);
                }
            }
            resSet.close();
            connection.close();
            statement.close();
            resultSet.close();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        System.out.println("Total cost of this project(only salaries of employees) is "+ project.getTotalCost());
    }
}
