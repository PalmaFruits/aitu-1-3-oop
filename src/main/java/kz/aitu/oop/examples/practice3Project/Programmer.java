package kz.aitu.oop.examples.practice3Project;

public class Programmer extends Employee{
    private String degree;

    public Programmer() {
    }

    public Programmer(String name, int salary, String degree) {
        super(name, salary);
        this.degree = degree;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) { //I forgot to add this in my UML
        this.degree = degree;
    }
}
