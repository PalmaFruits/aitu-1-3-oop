package kz.aitu.oop.examples.practice3Project;

public class Manager extends Employee{
    private String type;

    public Manager(String name, int salary, String type) {
        super(name, salary);
        this.type = type;
    }

    public Manager() {
    }

    public String getType() {
        return type;
    }
}
