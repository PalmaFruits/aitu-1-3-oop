package kz.aitu.oop.examples.practice3Project;

import java.util.ArrayList;
import java.util.List;

public class Project {
    private String title;
    private List<Employee> employeeList;
    public Project(String title) {
        this.title = title;
        employeeList=new ArrayList<>();
    }

    public Project() {
        employeeList=new ArrayList<>();
    }

    public String getTitle() {
        return title;
    }

    public void addEmployee(Employee e){
        employeeList.add(e);
    }

    public int getTotalCost(){
        int sum=0;
        for (int i=0;i<employeeList.size();i++){
            sum+=employeeList.get(i).getSalary();
        }
        return sum;
    }
}
