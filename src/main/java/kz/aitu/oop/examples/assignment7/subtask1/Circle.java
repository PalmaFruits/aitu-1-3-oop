package kz.aitu.oop.examples.assignment7.subtask1;

public class Circle extends shape{
    protected double radius=1.0;

    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }


    public Circle(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
    @Override
    double getArea() {
        return radius*radius*3.14;
    }

    @Override
    double getPerimetr() {
        return 2*3.14*radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", color='" + color + '\'' +
                ", filled=" + filled +
                '}';
    }
}
