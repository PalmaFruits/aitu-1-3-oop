package kz.aitu.oop.examples.assignment7.subtask3;

import static java.lang.Math.sqrt;

public class ResizableCircle extends Circle implements Resizable {

    public ResizableCircle(double radius) {
        super(radius);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public void resize(int percent) {
        radius=(getArea()*((double)percent/100))/3.14;
        radius=sqrt(radius);
    }
}
