package kz.aitu.oop.examples.assignment7.subtask1;

public abstract class shape {
    protected String color="red";
    protected boolean filled=true;

    public shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public shape() {
    }

    public String getColor() {
        return color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }
    abstract double getArea();
    abstract double getPerimetr();
    @Override
    public String toString() {
        return "shape{" +
                "color='" + color + '\'' +
                ", filled=" + filled +
                '}';
    }
}
