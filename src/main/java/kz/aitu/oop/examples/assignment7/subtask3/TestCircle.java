package kz.aitu.oop.examples.assignment7.subtask3;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/assignment7")

public class TestCircle {
    @GetMapping("/subtask3/TestCircle")
    public static void main(String[] args){
        GeometricObject a=new Circle(5.0);
        System.out.println(a.getArea());
        System.out.println(a.getPerimeter());
    }
}
