package kz.aitu.oop.examples.assignment7.subtask1;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/assignment7")

public class Main {
    @GetMapping("/subtask1")
    public static void main(String[] args){
        shape s1 = new Circle("red", false, 5.5); // Upcast Circle to shape
        System.out.println(s1); // which version? circle
        System.out.println(s1.getArea()); // which version? area finded because shape has this function
        System.out.println(s1.getPerimetr()); // which version? like in previous line but perimeter
        System.out.println(s1.getColor());
        System.out.println(s1.isFilled());
      //  System.out.println(s1.getRadius()); // error because be didn't downcasted to circle
        /*-----------------------------------------------*/
        Circle c1 = (Circle)s1; // Downcast back to Circle
        System.out.println(c1);
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimetr());
        System.out.println(c1.getColor());
        System.out.println(c1.isFilled());
        System.out.println(c1.getRadius()); // is true because it has downcasted
        /*-----------------------------------------------------*/

        //shape s2 = new shape();// error because our shape is abstract class. Abstract class never intends
        /*-------------------------------------------------------*/
        shape s3 = new Rectangle("red", false,1.0, 2.0 ); // Upcast
        System.out.println(s3);
        System.out.println(s3.getArea());
        System.out.println(s3.getPerimetr());
        System.out.println(s3.getColor());
        //System.out.println(s3.getLength()); // because of our rectangle upcasted to shape, shape hasn't any getLength
        /*-----------------------------------------------------------*/
        Rectangle r1 = (Rectangle)s3; // downcast
        System.out.println(r1);
        System.out.println(r1.getArea());
        System.out.println(r1.getColor());
        System.out.println(r1.getLength());// now it has length, because it downcasted to Rectangle
        /*---------------------------------------------------------*/
        shape s4 = new Square(6.6); // Upcast
        System.out.println(s4);
        System.out.println(s4.getArea());
        System.out.println(s4.getColor());
        //System.out.println(s4.getSide());//error because it upcasted to shape

// Take note that we downcast shape  s4 to Rectangle,
// which is a superclass of Square, instead of Square
        /*-----------------------------------------*/
        Rectangle r2 = (Rectangle)s4;
        System.out.println(r2);
        System.out.println(r2.getArea());
        System.out.println(r2.getColor());
        //System.out.println(r2.getSide());// it downcasted but to shape, it should be downcasted to square for having getSide
        System.out.println(r2.getLength());
// Downcast Rectangle r2 to Square
        Square sq1 = (Square)r2;
        System.out.println(sq1);
        System.out.println(sq1.getArea());
        System.out.println(sq1.getColor());
        System.out.println(sq1.getSide()); //now it is true because it downcasted to square
        System.out.println(sq1.getLength());

    }
}
