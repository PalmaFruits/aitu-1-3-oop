package kz.aitu.oop.examples.assignment7.subtask3;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/assignment7")

public class TestResizableCircle {
    @GetMapping("/subtask3/TestResCircle")
    public static void main(String[] args){
        Resizable a=new ResizableCircle(5.0);
        a.resize(50);
        System.out.println(a.toString());
    }
}
