package kz.aitu.oop.examples.assignment7.subtask2;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/assignment7")
public class Main {
    @GetMapping("/subtask2")
    public static void main(String[] args){
        Movable a= new MovablePoint(5,4,2,3);
        a.moveUp();
        System.out.println(a.toString());
        Movable b= new MovableCircle(4,5,6,7,25);
        b.moveLeft();
        System.out.println(b.toString());
    }
}
