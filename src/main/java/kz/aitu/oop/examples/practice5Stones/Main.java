package kz.aitu.oop.examples.practice5Stones;

import java.sql.*;

public class Main {

    public static void main(String[] args) {
        String url="jdbc:mysql://localhost:3306/pract5";
        String username="Didar";
        String password="d1freez14";
        Necklace necklace = new Necklace();
         try {
             Connection connection = DriverManager.getConnection(url,username,password);
             Statement statement = connection.createStatement();
             //let's get all stones in our necklace with id=1
             ResultSet resultSet = statement.executeQuery("select color,costPerCarat,pleochrism,name,weight from necklace join necklaceorder n on necklace.id = n.neckId join order1 o on n.orderId = o.id join stone s on o.stone_id = s.id where n.neckId=1");
             while (resultSet.next()){
                 if(resultSet.getString("color")==null)/*if color is null, it means that our stone is precious*/{
                /*Create Stone*/         Stone stone = new Precious(resultSet.getString("name"),resultSet.getInt("costPerCarat"),resultSet.getString("pleochrism"));
                /*Create Order*/         Order order = new Order (stone,resultSet.getInt("weight"));
                /*Order this stone*/         necklace.orderStone(order);

                 }
                 else /*else it is semiPrecious*/{
                     Stone stone = new SemiPrecious(resultSet.getString("name"),resultSet.getInt("costPerCarat"),resultSet.getString("color"));
                     Order order = new Order (stone,resultSet.getInt("weight"));
                     necklace.orderStone(order);}
                 }
             connection.close();
             statement.close();
             resultSet.close();

         } catch (SQLException e) {
             e.printStackTrace();
         }
        System.out.println("Total weight of our necklace "+necklace.getWeight());//this is total weight of our necklace
    }
}
