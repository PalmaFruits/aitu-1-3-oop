package kz.aitu.oop.examples.practice5Stones;

public class SemiPrecious extends Stone {
    private String color;

    public SemiPrecious(String name, int costPerCarat, String color) {
        super(name, costPerCarat);
        this.color = color;
    }

    public SemiPrecious() {
    }

    public String getColor() {
        return color;
    }


}
