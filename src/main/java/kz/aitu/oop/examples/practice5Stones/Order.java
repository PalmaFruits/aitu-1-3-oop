package kz.aitu.oop.examples.practice5Stones;

public class Order {
    private Stone stone;
    private int weight;

    public Order() {
    }

    public Order(Stone stone, int weight) {
        this.stone = stone;
        this.weight = weight;
    }


    public int getWeight() {
        return weight;
    }

    public Stone getStone() {
        return stone;
    }
}
