package kz.aitu.oop.examples.practice5Stones;

public class Precious extends Stone {
    private String pleochrism;

    public Precious(String name, int costPerCarat, String pleochrism) {
        super(name, costPerCarat);
        this.pleochrism = pleochrism;
    }

    public Precious() {
    }

    public String getPleochrism() {
        return pleochrism;
    }
}
