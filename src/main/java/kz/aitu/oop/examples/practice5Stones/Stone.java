package kz.aitu.oop.examples.practice5Stones;

public class Stone {
    private String name; //in uml i did one mistake with this, i forgot to change name's type to String after pasting it
    private int costPerCarat;

    public Stone(String name, int costPerCarat) {
        this.name = name;
        this.costPerCarat = costPerCarat;
    }

    public Stone() {
    }


    public String getName() {
        return name;
    }

    public int getCostPerCarat() {
        return costPerCarat;
    }
}
