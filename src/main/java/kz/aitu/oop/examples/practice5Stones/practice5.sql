-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Версия сервера: 10.4.11-MariaDB
-- Версия PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `pract5`
--

-- --------------------------------------------------------

--
-- Структура таблицы `necklace`
--

CREATE TABLE `necklace` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `necklace`
--

INSERT INTO `necklace` (`id`) VALUES
(1);

-- --------------------------------------------------------

--
-- Структура таблицы `necklaceorder`
--

CREATE TABLE `necklaceorder` (
  `neckId` int(11) NOT NULL,
  `orderId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `necklaceorder`
--

INSERT INTO `necklaceorder` (`neckId`, `orderId`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `order1`
--

CREATE TABLE `order1` (
  `id` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  `stone_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `order1`
--

INSERT INTO `order1` (`id`, `weight`, `stone_id`) VALUES
(1, 14, 2),
(2, 11, 6),
(3, 25, 2),
(4, 45, 3),
(5, 10, 8);

-- --------------------------------------------------------

--
-- Структура таблицы `stone`
--

CREATE TABLE `stone` (
  `id` int(11) NOT NULL,
  `costPerCarat` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `color` varchar(255) DEFAULT NULL,
  `pleochrism` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `stone`
--

INSERT INTO `stone` (`id`, `costPerCarat`, `name`, `color`, `pleochrism`) VALUES
(1, 45000, 'sapfire', 'red', NULL),
(2, 250000, 'diamond', NULL, 'antikvarnoe'),
(3, 8900, 'ruby', 'green', NULL),
(4, 12000, 'Tourmaline', 'Yellow', NULL),
(5, 23000, 'Garnet', 'green', NULL),
(6, 170000, 'Amber', NULL, 'strong'),
(7, 330000, 'Chalcedony', NULL, 'wind'),
(8, 42000, 'Beads', 'blue', NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `necklace`
--
ALTER TABLE `necklace`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `necklaceorder`
--
ALTER TABLE `necklaceorder`
  ADD KEY `FKneckId` (`neckId`),
  ADD KEY `FKorder` (`orderId`);

--
-- Индексы таблицы `order1`
--
ALTER TABLE `order1`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKtasId` (`stone_id`);

--
-- Индексы таблицы `stone`
--
ALTER TABLE `stone`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `necklace`
--
ALTER TABLE `necklace`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `order1`
--
ALTER TABLE `order1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `stone`
--
ALTER TABLE `stone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `necklaceorder`
--
ALTER TABLE `necklaceorder`
  ADD CONSTRAINT `FKneckId` FOREIGN KEY (`neckId`) REFERENCES `necklace` (`id`),
  ADD CONSTRAINT `FKorder` FOREIGN KEY (`orderId`) REFERENCES `order1` (`id`);

--
-- Ограничения внешнего ключа таблицы `order1`
--
ALTER TABLE `order1`
  ADD CONSTRAINT `FKtasId` FOREIGN KEY (`stone_id`) REFERENCES `stone` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
