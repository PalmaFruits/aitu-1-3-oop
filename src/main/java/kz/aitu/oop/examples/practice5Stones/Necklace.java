package kz.aitu.oop.examples.practice5Stones;

import java.util.ArrayList;
import java.util.List;

public class Necklace {
    private List<Order> orders;

    public Necklace() {
        this.orders = new ArrayList<Order>();
    }

    public int getCost(){
        int sum=0;
        for(int i=0;i<orders.size();i++){
            sum+=orders.get(i).getStone().getCostPerCarat();
        }
        return sum;
    }

    public void orderStone(Order order){
        orders.add(order);
    }

    public int getWeight(){
        int ans=0;
        for (int i=0;i<orders.size();i++){
            ans+=orders.get(i).getWeight();
        }
        return ans;
    }

    public void displayAllStones(){
        for (int i=0;i<orders.size();i++){
            System.out.println(orders.get(i).getStone().getName()+" "+orders.get(i).getWeight()+" carates");
        }
    }

    public void displayAllPrecious(){
        for (int i=0;i<orders.size();i++){
            if (orders.get(i).getStone() instanceof Precious)
            System.out.println(orders.get(i).getStone().getName()+" "+orders.get(i).getWeight()+" carates");
        }
    }

    public void displayAllSemiPrecious(){
        for (int i=0;i<orders.size();i++){
            if (orders.get(i).getStone() instanceof SemiPrecious)
                System.out.println(orders.get(i).getStone().getName()+" "+orders.get(i).getWeight()+" carates");
        }
    }
}
