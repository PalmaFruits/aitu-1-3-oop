package kz.aitu.oop.examples.practice7Quiz;

interface Food {
    String getType();
}


class Pizza implements Food {
    public String getType() {
        return "Someone ordered a Fast Food!";
    }
}

class Cake implements Food {
    public String getType() {
        return "Someone ordered a Dessert!";
    }
}


class FoodFactory {
    public Food getFood(String order) {
        if (order.equalsIgnoreCase("pizza")) { return new Pizza(); }
        else if (order.equalsIgnoreCase("cake")) { return new Cake(); }
        return new Pizza();
    }
}