package kz.aitu.oop.examples.practice2Trains;

import java.sql.*;

public class Main {
    public static void main(String[] args) {
        String url="jdbc:mysql://localhost:3306/pract2";
        String username="Didar";
        String password="d1freez14";
        Train train = null;
        Locomative locomative = new Locomative();
        /*Let's set our locomative*/
        try {
            Connection connection = DriverManager.getConnection(url,username,password);
            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery("select * from train join locomative on train.loc_id = locomative.id");

            if (resultSet.next()){
             locomative = new Locomative(resultSet.getString("name"),resultSet.getString("captain"));}
            train = Train.getInstance(locomative);
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            Connection connection1 = DriverManager.getConnection(url,username,password);
            Statement statement1 = connection1.createStatement();

            ResultSet resultSet1 = statement1.executeQuery("select pasID,FreightId from train join trainwagon on train.id = trainwagon.TrainId join wagon on wagon.id = trainwagon.WagonId");

            while (resultSet1.next()){
                //if Pas_id in our wagon table is not null, it is passanger wagon
                if (resultSet1.getBoolean("pasID") ) {
                    Connection connection2 = DriverManager.getConnection(url,username,password);
                    Statement statement2 = connection2.createStatement();

                    ResultSet resSet = statement2.executeQuery("select pasID,type,capacity,color from passangerwagon join wagon w on passangerwagon.id = w.pasID");

                    while (resSet.next()){
                        //if it is wagon which we are searching add it to our train
                        if (resSet.getInt("pasID")==resultSet1.getInt("pasID")){
                            Wagon a=new PassangerWagon(resSet.getString("color"),resSet.getString("type"),resSet.getInt("capacity"));
                            train.addWagon(a);
                            break;
                        }
                    }
                    resSet.close();
                    connection2.close();
                    statement2.close();
                }
                //else it is Freight wagon
                else {
                    Connection connection2 = DriverManager.getConnection(url,username,password);
                    Statement statement2 = connection2.createStatement();



                    ResultSet resSet = statement2.executeQuery("select FreightId,color,forWhat from freightwagon join wagon w on freightwagon.id = w.FreightId");

                    while (resSet.next()){
                        if (resSet.getInt("FreightId")==resultSet1.getInt("FreightId")){
                            Wagon a=new FreightWagon(resSet.getString("color"),resSet.getString("forWhat"));
                            train.addWagon(a);
                            break;
                        }
                    }
                    resSet.close();
                    connection2.close();
                    statement2.close();
                }

            }
            resultSet1.close();
            statement1.close();
            connection1.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
            System.out.println("Total capacity of passangers: "+train.getCapacity());

    }

}
