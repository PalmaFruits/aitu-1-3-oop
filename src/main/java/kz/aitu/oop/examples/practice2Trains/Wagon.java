package kz.aitu.oop.examples.practice2Trains;

public class Wagon {
    private String color;

    public Wagon(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
