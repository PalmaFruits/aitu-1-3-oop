package kz.aitu.oop.examples.practice2Trains;

public class PassangerWagon extends Wagon{
    private String type;
    private int capacity;

    public PassangerWagon(String color, String type, int capacity) {
        super(color);
        this.type = type;
        this.capacity = capacity;
    }

    public String getType() {
        return type;
    }

    public int getCapacity() {
        return capacity;
    }
}
