package kz.aitu.oop.examples.practice2Trains;

public class FreightWagon extends Wagon {
    public String forWhat;

    public FreightWagon(String color, String forWhat) {
        super(color);
        this.forWhat = forWhat;
    }


    public String getForWhat() {
        return forWhat;
    }
}
