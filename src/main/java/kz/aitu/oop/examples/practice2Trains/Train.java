package kz.aitu.oop.examples.practice2Trains;

import java.util.ArrayList;
import java.util.List;

public class Train {
    private Locomative locomative;
    private List<Wagon> wagonList;

    private static Train instance=null;

    private Train(Locomative locomative) {
        this.locomative = locomative;
        this.wagonList = new ArrayList<Wagon>();
    }

    public static Train getInstance(Locomative locomative) {
      if(instance==null) instance = new Train(locomative);
          return instance;
    }

    public void addWagon(Wagon a){
        wagonList.add(a);
    }

    public Train() {
    }

    public Wagon getWagon(int index){
        try
        {return wagonList.get(index);}
        catch (Exception e){
            System.out.println("Out of the train");
            return null;
        }
    }


    public Locomative getLocomative() {
        return locomative;
    }

    public void setLocomative(Locomative locomative) {
        this.locomative = locomative;
    }

    public int getCapacity(){
        int sum=0;
        for(int i=0;i<wagonList.size();i++ ){
            if(wagonList.get(i) instanceof PassangerWagon) {
               PassangerWagon a =  (PassangerWagon) wagonList.get(i);
               sum+=a.getCapacity();
            }
        }
        return sum;
    }
}
