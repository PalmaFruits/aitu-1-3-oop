package kz.aitu.oop.examples.practice2Trains;

public class Locomative {
    private String name;
    private String captain;

    public Locomative(String name, String captain) {
        this.name = name;
        this.captain = captain;
    }

    public Locomative() {
    }


    public String getName() {
        return name;
    }

    public String getCaptain() {
        return captain;
    }
}
