-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Версия сервера: 10.4.11-MariaDB
-- Версия PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `pract2`
--

-- --------------------------------------------------------

--
-- Структура таблицы `freightwagon`
--

CREATE TABLE `freightwagon` (
  `id` int(11) NOT NULL,
  `color` varchar(255) NOT NULL,
  `forWhat` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `freightwagon`
--

INSERT INTO `freightwagon` (`id`, `color`, `forWhat`) VALUES
(1, 'red', 'cars'),
(2, 'blue', 'cars'),
(3, 'yellow', 'electronics'),
(4, 'red', 'tables');

-- --------------------------------------------------------

--
-- Структура таблицы `locomative`
--

CREATE TABLE `locomative` (
  `id` int(11) NOT NULL,
  `captain` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `locomative`
--

INSERT INTO `locomative` (`id`, `captain`, `name`) VALUES
(1, 'Jack Harlow', 'Titanic'),
(2, 'Kendrick Lamar', 'Jabrail'),
(3, 'Travis Scott', 'Astro');

-- --------------------------------------------------------

--
-- Структура таблицы `passangerwagon`
--

CREATE TABLE `passangerwagon` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `capacity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `passangerwagon`
--

INSERT INTO `passangerwagon` (`id`, `type`, `color`, `capacity`) VALUES
(1, 'lux', 'yellow', 40),
(2, 'lux', 'red', 20),
(3, 'eco', 'yellow', 100),
(4, 'plac', 'blue', 160),
(5, 'eco', 'red', 80),
(6, 'plac', 'yellow', 150),
(7, 'eco', 'blue', 130);

-- --------------------------------------------------------

--
-- Структура таблицы `train`
--

CREATE TABLE `train` (
  `id` int(11) NOT NULL,
  `loc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `train`
--

INSERT INTO `train` (`id`, `loc_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `trainwagon`
--

CREATE TABLE `trainwagon` (
  `WagonId` int(11) NOT NULL,
  `TrainId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `trainwagon`
--

INSERT INTO `trainwagon` (`WagonId`, `TrainId`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `wagon`
--

CREATE TABLE `wagon` (
  `id` int(11) NOT NULL,
  `pasID` int(11) DEFAULT NULL,
  `FreightId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `wagon`
--

INSERT INTO `wagon` (`id`, `pasID`, `FreightId`) VALUES
(1, 1, NULL),
(2, NULL, 1),
(3, 2, NULL),
(4, 3, NULL),
(5, NULL, 2),
(6, NULL, 3),
(7, 4, NULL),
(8, 5, NULL),
(9, 6, NULL),
(10, NULL, 4),
(11, 7, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `freightwagon`
--
ALTER TABLE `freightwagon`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `locomative`
--
ALTER TABLE `locomative`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `passangerwagon`
--
ALTER TABLE `passangerwagon`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `train`
--
ALTER TABLE `train`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Locomative1` (`loc_id`);

--
-- Индексы таблицы `trainwagon`
--
ALTER TABLE `trainwagon`
  ADD KEY `FK_train` (`TrainId`),
  ADD KEY `FK_wagon` (`WagonId`);

--
-- Индексы таблицы `wagon`
--
ALTER TABLE `wagon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pasID` (`pasID`),
  ADD KEY `FreightId` (`FreightId`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `freightwagon`
--
ALTER TABLE `freightwagon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `locomative`
--
ALTER TABLE `locomative`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `passangerwagon`
--
ALTER TABLE `passangerwagon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `train`
--
ALTER TABLE `train`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `train`
--
ALTER TABLE `train`
  ADD CONSTRAINT `FK_Locomative1` FOREIGN KEY (`loc_id`) REFERENCES `locomative` (`id`);

--
-- Ограничения внешнего ключа таблицы `trainwagon`
--
ALTER TABLE `trainwagon`
  ADD CONSTRAINT `FK_train` FOREIGN KEY (`TrainId`) REFERENCES `train` (`id`),
  ADD CONSTRAINT `FK_wagon` FOREIGN KEY (`WagonId`) REFERENCES `wagon` (`id`);

--
-- Ограничения внешнего ключа таблицы `wagon`
--
ALTER TABLE `wagon`
  ADD CONSTRAINT `wagon_ibfk_1` FOREIGN KEY (`pasID`) REFERENCES `passangerwagon` (`id`),
  ADD CONSTRAINT `wagon_ibfk_2` FOREIGN KEY (`FreightId`) REFERENCES `freightwagon` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
