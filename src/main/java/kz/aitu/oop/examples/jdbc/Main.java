package kz.aitu.oop.examples.jdbc;

import java.sql.*;

public class Main {
    public static void main(String[] args) {
        String url = "jdbc:mysql://localhost:3306/testMyDB";
        String username = "Didar";
        String password = "d1freez14";


        try {
            Connection connection = DriverManager.getConnection(url,username,password);
            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery("select * from product");
            while(resultSet.next()){
                System.out.println(resultSet.getString("name" ));
            }
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
