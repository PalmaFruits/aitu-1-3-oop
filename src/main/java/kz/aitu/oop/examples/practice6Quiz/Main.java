package kz.aitu.oop.examples.practice6Quiz;

import java.util.Scanner;


class Singleton{
    private static Singleton instance = null;
    public String str;
    private Singleton() {}
    public static Singleton getSingleInstance() {
        if (instance == null) {
            instance = new Singleton();
            Scanner sc = new Scanner(System.in);
            String input = "";
                input += sc.nextLine();
            instance.str = String.format("Hello I am a singleton! Let me say %s"
                    + " to you", input);
            System.out.println(instance.str);
        }
        return instance;
    }
}

public class Main{
    public static void main(String[] args) {
        Singleton.getSingleInstance();
    }
}
