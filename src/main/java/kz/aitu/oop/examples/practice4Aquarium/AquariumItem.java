package kz.aitu.oop.examples.practice4Aquarium;

public class AquariumItem {
    private String name;
    private int cost;

    public AquariumItem(String name, int cost) {
        this.name = name;
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public int getCost() {
        return cost;
    }
}
