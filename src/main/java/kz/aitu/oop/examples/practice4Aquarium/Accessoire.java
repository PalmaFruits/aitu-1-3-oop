package kz.aitu.oop.examples.practice4Aquarium;

public class Accessoire extends AquariumItem{
    private String forWhat;

    public String getForWhat() {
        return forWhat;
    }

    public Accessoire(String name, int cost, String forWhat) {
        super(name, cost);
        this.forWhat = forWhat;
    }
}
