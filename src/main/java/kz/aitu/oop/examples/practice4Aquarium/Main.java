package kz.aitu.oop.examples.practice4Aquarium;

import java.sql.*;

public class Main {
    public static void main(String[] args) {
        String url="jdbc:mysql://localhost:3306/pract4";
        String username="Didar";
        String password="d1freez14";
        Aquarium aquarium = new Aquarium();
        try{
            Connection connection = DriverManager.getConnection(url,username,password);
            Statement statement = connection.createStatement();
            // let's  add all animals in our first aquarium
            ResultSet resultSet = statement.executeQuery("select name,cost,squad,species,weight,prefferedWater from animal join aquariumitem on " +
                    " animal.id = aquariumitem.anim_id join itemaquarium i on aquariumitem.id = i.item_id join aquarium a on i.aqua_id = a.id where aqua_id=1");
            while (resultSet.next()){
                if(resultSet.getString("squad")==null){//it is fish
                    AquariumItem aquariumItem= new Fish(resultSet.getString("name"),resultSet.getInt("cost"),resultSet.getInt("weight"),resultSet.getString("species"),resultSet.getString("prefferedWater"));
                    aquarium.addItem(aquariumItem);
                }
                else {
                    AquariumItem aquariumItem= new Reptile(resultSet.getString("name"),resultSet.getInt("cost"),resultSet.getInt("weight"),resultSet.getString("species"),resultSet.getString("squad"));
                    aquarium.addItem(aquariumItem);
                }
            }
            // let's add all accessoire
            ResultSet resSet = statement.executeQuery("select name,cost,forWhat from accessoire join aquariumitem on accessoire.id = aquariumitem.acc_id " +
                    "join itemaquarium i on aquariumitem.id = i.item_id join aquarium a on i.aqua_id = a.id where aqua_id=1");
            while (resSet.next()){
                AquariumItem aquariumItem = new Accessoire(resSet.getString("name"),resSet.getInt("cost"),resSet.getString("forWhat"));
                aquarium.addItem(aquariumItem);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        aquarium.displayAll();
    }
}
