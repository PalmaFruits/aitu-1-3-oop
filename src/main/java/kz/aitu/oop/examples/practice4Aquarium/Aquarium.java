package kz.aitu.oop.examples.practice4Aquarium;

import java.util.ArrayList;
import java.util.List;

public class Aquarium {
    private List<AquariumItem> items;

    Aquarium(){
        items=new ArrayList<>();
    }
    public void addItem(AquariumItem e){
        items.add(e);
    }
    public void displayAllAnimal(){
        for (int i=0;i<items.size();i++){
            if(items.get(i) instanceof Animal){
                System.out.println(items.get(i).getName()+" of species "+((Animal) items.get(i)).getSpecies()
                +" and weight is "+((Animal) items.get(i)).getWeight());}
        }
    }

    public void displayAllAccessoire(){
        for (int i=0;i<items.size();i++){
            if (items.get(i) instanceof Accessoire){
                System.out.println(items.get(i).getName()+" is used for "+((Accessoire) items.get(i)).getForWhat());
            }
        }
    }

    public void displayAll(){
        for (int i=0;i<items.size();i++){
            if (items.get(i) instanceof Accessoire){
                System.out.println(items.get(i).getName()+" is used for "+((Accessoire) items.get(i)).getForWhat());
            }
            else{
                System.out.println(items.get(i).getName()+" of species "+((Animal) items.get(i)).getSpecies()
                        +" and weight is "+((Animal) items.get(i)).getWeight());
            }
        }
    }

    public int getCost(){
        int sum=0;
        for(int i=0;i<items.size();i++){
            sum+=items.get(i).getCost();
        }
        return sum;
    }
}
