package kz.aitu.oop.examples.practice4Aquarium;

public class Animal extends AquariumItem{
    private int weight;
    private String species;

    public Animal(String name, int cost, int weight, String species) {
        super(name, cost);
        this.weight = weight;
        this.species = species;
    }

    public String getSpecies() {
        return species;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
