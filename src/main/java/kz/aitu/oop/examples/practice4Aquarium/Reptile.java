package kz.aitu.oop.examples.practice4Aquarium;

public class Reptile extends Animal{ // Reptile, not Reptiles))
    private String squad;

    public String getSquad() {
        return squad;
    }

    public Reptile(String name, int cost, int weight, String species, String squad) {
        super(name, cost, weight, species);
        this.squad = squad;
    }
}
