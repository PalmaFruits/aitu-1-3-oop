package kz.aitu.oop.examples.practice4Aquarium;

public class Fish extends Animal{
    private String prefferedWater;

    public Fish(String name, int cost, int weight, String species, String prefferedWater) {
        super(name, cost, weight, species);
        this.prefferedWater = prefferedWater;
    }

    public String getPrefferedWater() {
        return prefferedWater;
    }
}
