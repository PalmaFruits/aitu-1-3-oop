-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Версия сервера: 10.4.11-MariaDB
-- Версия PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `pract4`
--

-- --------------------------------------------------------

--
-- Структура таблицы `accessoire`
--

CREATE TABLE `accessoire` (
  `id` int(11) NOT NULL,
  `forWhat` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `accessoire`
--

INSERT INTO `accessoire` (`id`, `forWhat`) VALUES
(1, 'giving food'),
(2, 'cleaning water');

-- --------------------------------------------------------

--
-- Структура таблицы `animal`
--

CREATE TABLE `animal` (
  `id` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  `species` varchar(255) NOT NULL,
  `prefferedWater` varchar(255) DEFAULT NULL,
  `squad` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `animal`
--

INSERT INTO `animal` (`id`, `weight`, `species`, `prefferedWater`, `squad`) VALUES
(1, 15, 'karas', 'saulty', NULL),
(2, 25, 'varane', NULL, 'footies'),
(3, 5, 'nemo', 'no saulty', NULL),
(4, 13, 'chameleon', NULL, 'footies'),
(5, 6, 'dory', 'no saulty', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `aquarium`
--

CREATE TABLE `aquarium` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `aquarium`
--

INSERT INTO `aquarium` (`id`) VALUES
(1);

-- --------------------------------------------------------

--
-- Структура таблицы `aquariumitem`
--

CREATE TABLE `aquariumitem` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `cost` int(11) NOT NULL,
  `anim_id` int(11) DEFAULT NULL,
  `acc_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `aquariumitem`
--

INSERT INTO `aquariumitem` (`id`, `name`, `cost`, `anim_id`, `acc_id`) VALUES
(1, 'Beluga', 25000, 5, NULL),
(2, 'GTX 5005', 6300, NULL, 1),
(3, 'Koshka', 105000, 2, NULL),
(4, 'DoryDans', 65000, 5, NULL),
(5, 'Kabdyr', 41000, 3, NULL),
(6, 'Plus 7', 8000, NULL, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `itemaquarium`
--

CREATE TABLE `itemaquarium` (
  `item_id` int(11) NOT NULL,
  `aqua_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `itemaquarium`
--

INSERT INTO `itemaquarium` (`item_id`, `aqua_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `accessoire`
--
ALTER TABLE `accessoire`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `animal`
--
ALTER TABLE `animal`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aquarium`
--
ALTER TABLE `aquarium`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aquariumitem`
--
ALTER TABLE `aquariumitem`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkforanim` (`anim_id`),
  ADD KEY `fkForAcc` (`acc_id`);

--
-- Индексы таблицы `itemaquarium`
--
ALTER TABLE `itemaquarium`
  ADD KEY `it_id` (`item_id`),
  ADD KEY `aqua_id` (`aqua_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `accessoire`
--
ALTER TABLE `accessoire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `animal`
--
ALTER TABLE `animal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `aquarium`
--
ALTER TABLE `aquarium`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `aquariumitem`
--
ALTER TABLE `aquariumitem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `aquariumitem`
--
ALTER TABLE `aquariumitem`
  ADD CONSTRAINT `fkForAcc` FOREIGN KEY (`acc_id`) REFERENCES `accessoire` (`id`),
  ADD CONSTRAINT `fkforanim` FOREIGN KEY (`anim_id`) REFERENCES `animal` (`id`);

--
-- Ограничения внешнего ключа таблицы `itemaquarium`
--
ALTER TABLE `itemaquarium`
  ADD CONSTRAINT `aqua_id` FOREIGN KEY (`aqua_id`) REFERENCES `aquarium` (`id`),
  ADD CONSTRAINT `it_id` FOREIGN KEY (`item_id`) REFERENCES `aquariumitem` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
