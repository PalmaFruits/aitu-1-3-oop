package kz.aitu.oop.examples.schedule;

import lombok.Data;

@Data
public abstract class Human {
    private String fName;
    private String lName;
    public Human(String fName,String lName){
        this.fName=fName;
        this.lName=lName;
    }
}
