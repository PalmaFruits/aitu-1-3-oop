package kz.aitu.oop.examples.schedule;

import lombok.Data;

@Data
public class Time {
    private int startTime;
    private int finishTime;

    public Time(int startTime, int finishTime) {
        this.startTime = startTime;
        this.finishTime = finishTime;
    }
}
