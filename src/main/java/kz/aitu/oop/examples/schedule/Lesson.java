package kz.aitu.oop.examples.schedule;

import lombok.Data;

@Data
public class Lesson {
    private String day;
    private String lessonName;
    private Time time;
    private Teacher teacher;
    private Group group;
    private String cabinet;

    public Lesson(String day, String lessonName, Time time, Teacher teacher, Group group, String cabinet) {
        this.day = day;
        this.lessonName = lessonName;
        this.time = time;
        this.teacher = teacher;
        this.group = group;
        this.cabinet = cabinet;
    }
}
