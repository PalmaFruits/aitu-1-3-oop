package kz.aitu.oop.examples.schedule;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Group {
    private String groupName;
    private String faculty;
    private List<Student> students;

    public Group(String groupName, String faculty) {
        this.groupName = groupName;
        this.faculty = faculty;
        this.students = new ArrayList<Student>();
    }

    public void addStudent(Student student){
        students.add(student);
    }
}
