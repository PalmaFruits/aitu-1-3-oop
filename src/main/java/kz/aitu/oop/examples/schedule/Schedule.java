package kz.aitu.oop.examples.schedule;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
@Data
public class Schedule {
    private static List<Lesson> lessons;
    private static Schedule instance=null;
    private Schedule() {
        this.lessons = new ArrayList<Lesson>();
    }
    public  Schedule getInstance() {
        if(instance==null){
            instance = new Schedule();
        }
        return instance;
    }
    public void addLesson(Lesson lesson){
        lessons.add(lesson);
    }

    public void deleteLesson(Lesson lesson){
        lessons.remove(lesson);
    }
}
