package kz.aitu.oop.examples.schedule;

import lombok.Data;

@Data
public class Student extends Human{
    private String groupName;
    public Student(String fName, String lName, String groupName) {
        super(fName, lName);
        this.groupName = groupName;
    }

}
