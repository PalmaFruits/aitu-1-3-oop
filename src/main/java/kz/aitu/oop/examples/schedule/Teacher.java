package kz.aitu.oop.examples.schedule;

import lombok.Data;

@Data
public class Teacher extends Human{
    private int Salary;

    public Teacher(String fName, String lName, int salary) {
        super(fName, lName);
        Salary = salary;
    }
}
